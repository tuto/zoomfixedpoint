##Zoom in a fixed point

	Simple implementation for a canvas image zoom in a specific mouse point.
	This app use a matrix transforms based in SVG transform because the matrix transform
	for canvas html is basic.

##How this works
	
	- Download the code
	- Open in a browser the file index.html
	- Move the mouse wheel over a image point

##Example

	http://dakoo.cl/archivos/zoomFixedPoint/

